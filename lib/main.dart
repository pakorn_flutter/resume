import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget resumeSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: Text(
                  'ประวัติส่วนตัว',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
                ),
              ),
              Text(
                  'ชื่อ: นายภากร เต็บเตียง' +
                      '\n' +
                      'ชื่อเล่น: ปุณ   อายุ: 22 ปี' +
                      '\n' +
                      'เกิดวันที่: 20 กุมภาพันธ์ 2542' +
                      '\n' +
                      'ศาสนา: พุทธ  เชื้อชาติ: ไทย  สัญชาติ: ไทย' +
                      '\n' +
                      'กำลังศึกษาที่:' +
                      '\n' +
                      'มหาวิทยาลัยบูรพา วิทยาเขตบางแสน' +
                      '\n' +
                      'คณะวิทยาการสารสนเทศ' +
                      '\n' +
                      'สาขาวิทยาการคอมพิวเตอร์ (Computer Science)' +
                      '\n' +
                      'งานอดิเรก: เล่นเกม',
                  style: TextStyle(fontSize: 20))
            ],
          )),
        ],
      ),
    );
    Widget myskillSection = Container(
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: Text(
                  'My skill',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
                ),
              ),
            ],
          )),
        ],
      ),
    );
    Widget detailMyskill = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'image/Css.png',
            width: 80,
            height: 90,
          ),
          Image.asset(
            'image/HTML.png',
            width: 80,
            height: 90,
          ),
          Image.asset(
            'image/Flutter.png',
            width: 150,
            height: 150,
          ),
          Image.asset(
            'image/java.jpg',
            width: 80,
            height: 90,
          ),
          Image.asset(
            'image/Scratch.jpg',
            width: 140,
            height: 150,
          ),
          Image.asset(
            'image/Vue.png',
            width: 80,
            height: 90,
          ),
        ],
      ),
    );

    Widget myeducationSection = Container(
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: Text(
                  'My Education',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
                ),
              ),
            ],
          )),
        ],
      ),
    );
    Widget detailEducation = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Column(
              children: [
                Image.asset(
                  'image/school1.jpg',
                  width: 80,
                  height: 90,
                ),
                Text(
                  'ชั้นประถมศึกษา',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Image.asset(
                  'image/school2.jpg',
                  width: 80,
                  height: 90,
                ),
                Text(
                  'ชั้นมัธยมศึกษาตอนต้นและ' + '\n' + 'ชั้นมัธยมศึกษาตอนปลาย',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Image.asset(
                  'image/school3.jpg',
                  width: 80,
                  height: 90,
                ),
                Text(
                  'ระดับปริญญาตรี' + '\n' + '(กำลังศึกษา)',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return MaterialApp(
      title: 'Welcome to My Resume',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[600],
          leading: Icon(Icons.sentiment_very_satisfied,
              size: 40, color: Colors.black),
          title: const Text(
            'My Resume',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        body: ListView(children: [
          Image.asset(
            'image/Pun.jpg',
            width: 400,
            height: 300,
          ),
          resumeSection,
          myskillSection,
          detailMyskill,
          myeducationSection,
          detailEducation,
        ]),
        bottomNavigationBar: BottomAppBar(
          color: Colors.black,
          shape: const CircularNotchedRectangle(),
          child: Container(
              height: 50,
              child: Icon(
                Icons.headphones,
                size: 20,
              )),
        ),
      ),
    );
  }
}
